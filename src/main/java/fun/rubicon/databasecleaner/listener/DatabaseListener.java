package fun.rubicon.databasecleaner.listener;

import fun.rubicon.plugin.entities.RubiconGuild;
import fun.rubicon.plugin.entities.provider.GuildProvider;
import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

@Log4j
public class DatabaseListener extends ListenerAdapter {


    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        RubiconGuild rubiconGuild = GuildProvider.getGuild(event.getGuild().getIdLong());
        if(rubiconGuild != null) {
            rubiconGuild.delete(guild -> log.debug(String.format("[DB] Deleted guild: %s", guild.getGuildId())), error -> log.error("[DB] Could not delete guild", error));
        }
    }
}
