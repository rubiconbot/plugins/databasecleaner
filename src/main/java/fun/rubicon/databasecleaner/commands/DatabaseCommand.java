package fun.rubicon.databasecleaner.commands;

import com.datastax.driver.core.ResultSet;
import fun.rubicon.plugin.command.*;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.plugin.util.Colors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;

public class DatabaseCommand extends Command {

    public DatabaseCommand() {
        super(new String[] {"database", "db"}, CommandCategory.BOT_OWNER, "<query>", "Executes CQL queries.");
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        ResultSet res;
        try {
            res = Cassandra.getCassandra().getConnection().execute(event.getArgsAsString());
        } catch (Exception e){
            return send(buildResponse("An error occurred", event.getArgsAsString(), e.getMessage(), event.getAuthor()).setColor(Colors.RED));
        }
        return send(buildResponse("Executed statement",event.getArgsAsString(), res.one().toString(), event.getAuthor()).setColor(Colors.GREEN));
    }

    private EmbedBuilder buildResponse(String title, String input, String output, User author){
        return success(title, "Input: \n" +
                "```" +
                input + "```\n" +
                "Output: \n```" +
                output +
                "```")
                .setFooter("Requested by " +author .getName() + "#" + author.getDiscriminator(), author.getAvatarUrl());
    }
}
