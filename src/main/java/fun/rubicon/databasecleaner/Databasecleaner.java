package fun.rubicon.databasecleaner;

import fun.rubicon.databasecleaner.commands.DatabaseCommand;
import fun.rubicon.databasecleaner.listener.DatabaseListener;
import fun.rubicon.plugin.Plugin;

public class Databasecleaner extends Plugin {

    @Override
    public void init() {
        registerListener(new DatabaseListener());
        registerCommand(new DatabaseCommand());
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
